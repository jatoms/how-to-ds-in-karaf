# How to DS in Karaf
## What is DS?
* DI in OSGi
## How does it work?
* Annotations
* bnd
* Manifest + XML
* SCR
## Usecases
### Component without Service
### Component with Service
### Component with Configuration
### Component with Static/Reluctant/Mandatory Reference
### Component with Dynamic/Greedy/Multiple Reference
### Component with both types of Reference
### Scopes (Singleton/Per Bundle/Prototype)
### Component with Reference with target
### ServiceObjects / Factories
### "Deadlocks"
* Component to Component
* ServiceObjects to Component within same Bundle
### ???
